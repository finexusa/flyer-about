# FINEX About Flyer

1.0 - Initial commit

1.1 - Inverted color scheme
    - Altered footer color

1.2 - Removed - from easy release copy

1.3 - Renamed files

1.4 - Fixed bleed on image.